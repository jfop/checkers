#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>

using namespace std;

void boardGeneration(vector<vector<char> >&);
void hillClimb(vector<vector<char> >&);
void displayBoard(vector<vector<char> >&);
int main(int argc, char* argv[]) {

	vector < vector<char> > generatedboard(8, vector<char>(8, '-'));

	boardGeneration (generatedboard);
	hillClimb(generatedboard);

}

void displayBoard(vector<vector<char> >& generatedboard) {
	for (int q = 0; q < 8; q++) {
		for (int w = 0; w < 8; w++) {
			cout << generatedboard[q][w] << " ";
		}
		cout << endl;
	}
}
void boardGeneration(vector<vector<char> >& generatedboard) {

	//WHITE
	int x = 0;
	int y = 1;
	for (int i = 0; i <= 12; i++) {
		generatedboard[x][y] = 'W';
		if (y == 8) {
			x++;
			y = 1;
		} else if (y == 7) {
			x++;
			y = 0;
		} else {
			y = y + 2;
		}
	}
	//BLACK
	x = 5;
	y = 1;
	for (int i = 0; i <= 12; i++) {
		generatedboard[x][y] = 'B';
		if (y == 8) {
			x++;
			y = 1;
		} else if (y == 7) {
			x++;
			y = 0;
		} else {
			y = y + 2;
		}
	}
}
void hillClimb(vector<vector<char> >& generatedboard) {
	int playerturn = 0;
	bool movechange = true;

	while (movechange == true) {
		movechange = false;
		if (playerturn == 0) {
			for (int q = 0; q < 8; q++) {
				for (int w = 0; w < 8; w++) {
					if (generatedboard[q][w] == 'B'
							&& generatedboard[q + 1][w + 1] == 'W') {
						generatedboard[q][w] == '-';
						generatedboard[q + 1][w + 1] == '-';
						generatedboard[q + 2][w + 2] == 'B';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 1;
						movechange = true;
					}
					if (generatedboard[q][w] == 'B'
							&& generatedboard[q - 1][w - 1] == 'W') {
						generatedboard[q][w] == '-';
						generatedboard[q - 1][w - 1] == '-';
						generatedboard[q - 2][w - 2] == 'B';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 1;
						movechange = true;
					}
					if (generatedboard[q][w] == 'B'
							&& generatedboard[q + 1][w - 1] == 'W') {
						generatedboard[q][w] == '-';
						generatedboard[q + 1][w - 1] == '-';
						generatedboard[q + 2][w - 2] == 'B';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 1;
						movechange = true;
					}
					if (generatedboard[q][w] == 'B'
							&& generatedboard[q - 1][w + 1] == 'W') {
						generatedboard[q][w] == '-';
						generatedboard[q - 1][w + 1] == '-';
						generatedboard[q - 2][w + 2] == 'B';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 1;
						movechange = true;
					}
				}
			}
		}
		if (playerturn == 1) {
			for (int q = 0; q < 8; q++) {
				for (int w = 0; w < 8; w++) {
					if (generatedboard[q][w] == 'W'
							&& generatedboard[q + 1][w + 1] == 'B') {
						generatedboard[q][w] == '-';
						generatedboard[q + 1][w + 1] == '-';
						generatedboard[q + 2][w + 2] == 'W';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 0;
						movechange = true;
					}
					if (generatedboard[q][w] == 'W'
							&& generatedboard[q - 1][w - 1] == 'B') {
						generatedboard[q][w] == '-';
						generatedboard[q - 1][w - 1] == '-';
						generatedboard[q - 2][w - 2] == 'W';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 0;
						movechange = true;
					}
					if (generatedboard[q][w] == 'W'
							&& generatedboard[q + 1][w - 1] == 'B') {
						generatedboard[q][w] == '-';
						generatedboard[q + 1][w - 1] == '-';
						generatedboard[q + 2][w - 2] == 'W';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 0;
						movechange = true;
					}
					if (generatedboard[q][w] == 'W'
							&& generatedboard[q - 1][w + 1] == 'B') {
						generatedboard[q][w] == '-';
						generatedboard[q - 1][w + 1] == '-';
						generatedboard[q - 2][w + 2] == 'W';
						hillClimb(generatedboard);
						displayBoard(generatedboard);
						playerturn = 0;
						movechange = true;
					}
				}
			}
		}
	}
}
